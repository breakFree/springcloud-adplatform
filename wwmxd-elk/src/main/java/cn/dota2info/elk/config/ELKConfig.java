package cn.dota2info.elk.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ELKConfig {
    @Value("${ElkUrl}")
    private String elkUrl;
    @Value("${ELK-Port}")
    private Integer elkPort;

    @Bean
    public RestHighLevelClient restHighLevelClient(){
        return new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost(elkUrl, elkPort, "http")));
    }

}
