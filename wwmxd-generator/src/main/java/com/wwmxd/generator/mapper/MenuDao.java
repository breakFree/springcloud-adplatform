package com.wwmxd.generator.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.wwmxd.common.mapper.SuperMapper;
import com.wwmxd.generator.entity.Menu;

/**
 *
 * 
 *
 * @author WWMXD
 * @email 309980030@qq.com
 * @date 2018-01-02 16:21:35
 */
public interface MenuDao extends SuperMapper<Menu> {

    public int deleteall();
}